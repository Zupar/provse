# coding: utf-8
from django.conf import settings
from django.conf.urls import patterns, include, url
from blog.views import AdsListView

from django.contrib import admin
admin.autodiscover()  

urlpatterns = patterns('',
	url(r'^$', AdsListView.as_view(), name='list'), 
    url(r'^admin/', include(admin.site.urls)), 
	url(r'^categories/', include('blog.urls')),
)


urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
    'document_root': settings.MEDIA_ROOT}))
