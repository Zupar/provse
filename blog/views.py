#coding: utf-8
from blog.models import Ads 
from django.views.generic import ListView, DetailView

class AdsListView(ListView): # представление в виде списка
    model = Ads                   # модель для представления 

class AdsDetailView(DetailView): # детализированное представление модели
    model = Ads