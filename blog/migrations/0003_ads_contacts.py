# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20150209_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='ads',
            name='contacts',
            field=models.CharField(default=10, max_length=255),
            preserve_default=False,
        ),
    ]
