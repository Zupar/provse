# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_ads_contacts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ads',
            name='contacts',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435'),
            preserve_default=True,
        ),
    ]
