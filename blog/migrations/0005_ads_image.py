# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20150209_1453'),
    ]

    operations = [
        migrations.AddField(
            model_name='ads',
            name='image',
            field=models.ImageField(default=10, help_text=b'150x150px', verbose_name='\u0412\u0430\u0448\u0435 \u0444\u043e\u0442\u043e', upload_to=b'images/'),
            preserve_default=False,
        ),
    ]
