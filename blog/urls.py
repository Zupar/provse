#coding: utf-8
from django.conf.urls import patterns, url

from blog.views import AdsListView, AdsDetailView 

urlpatterns = patterns('',
url(r'^$', AdsListView.as_view(), name='list'), # то есть по URL http://имя_сайта/blog/ 
                                               # будет выводиться список постов
url(r'^(?P<pk>\d+)/$', AdsDetailView.as_view()), # а по URL http://имя_сайта/blog/число/ 
                                              # будет выводиться пост с определенным номером

)