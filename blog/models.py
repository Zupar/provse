# coding: utf-8
from django.db import models

class Ads(models.Model):
    title = models.CharField(max_length = 255 ) # заголовок поста
    image = models.ImageField(upload_to="images/", 
        verbose_name=u'Ваше фото', help_text='150x150px')
    datetime = models.DateTimeField(u'Дата публикации') # дата публикации
    content = models.TextField(max_length = 10000) # текст поста
    contacts = models.CharField(u'Контактные данные',max_length = 255)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/categories/%i" % self.id